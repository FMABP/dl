from os import environ

SESSION_CONFIGS = [
    dict(
        name='pilot',
        display_name='Pilot',
        num_demo_participants=1,
        app_sequence=['pilot'],
        payByTable=0.20,
        seedPractice="test",
        seedTask="banana",
        rows=6,
        columns=6,
        minValue=1,
        maxValue=9,
        timeGlobalPractice=240000, 
        timeRestPractice=60000,
        timeGlobal=2520000, 
        timeRest=120000, 
        colorOK="#19fc04",
        colorNotOK="red",

        doc="""
    Edit the seedTrial parameter to change the random number in trial table.
    </br>Edit the rows parameter to change number of rows in tables;
    </br>Edit the columns parameter to change number of columns in the table;
    </br>Edit the minValue parameter to change the minimum value in each cell of tables;
    </br>Edit the maxValue parameter to change the maximum value in each cell of tables;
    </br>Edit the timeGlobal parameter to change time total to do tasks and rests in miliseconds;
    </br>Edit the timeRest parameter to change time total to do rests in miliseconds;
    </br>Edit the colorOK parameter to change the color when participant result is correct each cell of tables;
    </br>Edit the colorNotOK parameter to change the color when participant result is correct each cell of tables;
    """
    ),
]

# if you set a property in SESSION_CONFIG_DEFAULTS, it will be inherited by all configs
# in SESSION_CONFIGS, except those that explicitly override it.
# the session config can be accessed from methods in your apps as self.session.config,
# e.g. self.session.config['participation_fee']

SESSION_CONFIG_DEFAULTS = dict(
    real_world_currency_per_point=1.00, participation_fee=0.00, doc=""
)

PARTICIPANT_FIELDS = []
SESSION_FIELDS = []

# ISO-639 code
# for example: de, fr, ja, ko, zh-hans
LANGUAGE_CODE = 'en'

# e.g. EUR, GBP, CNY, JPY
REAL_WORLD_CURRENCY_CODE = 'GBP'
USE_POINTS = False

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

DEMO_PAGE_INTRO_HTML = """ """

SECRET_KEY = '7987049207780'
