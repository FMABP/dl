from otree.api import *


doc = """
Pilot to deadline project
"""


class C(BaseConstants):
    NAME_IN_URL = 'pilot'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
 is_consent   = models.BooleanField()
 numbersTablesCorrect = models.IntegerField(blank=True)
 numberInteraction = models.IntegerField(blank=True)
 timeTable = models.LongStringField(blank=True)
 tableAllTableCorrect = models.LongStringField(blank=True)
 tableSumAllColumnsCorrect = models.LongStringField(blank=True)
 tableSumColumnsCorrect = models.LongStringField(blank=True)
 


# PAGES
class Consent(Page):
    form_model = 'player'
    form_fields = ['is_consent'] 
class Instructions(Page): 
    @staticmethod
    def js_vars(player):
        session =player.session
        return dict()

class TaskPractice(Page):
    @staticmethod
    def js_vars(player):
        session =player.session
        return dict(
            seedTask=session.config['seedPractice'],rows=session.config['rows'],columns=session.config['columns'],minValue=session.config['minValue'],maxValue=session.config['maxValue'],timeGlobal=session.config['timeGlobalPractice'],timeRest=session.config['timeRestPractice'],colorOK=session.config['colorOK'],colorNotOK=session.config['colorNotOK'],subjectID=player.id_in_subsession,
            
        )


class PreTask(Page): 
    pass

class Task(Page):
    form_model = 'player'
    form_fields = ['numbersTablesCorrect','numberInteraction','timeTable','tableAllTableCorrect','tableSumAllColumnsCorrect','tableSumColumnsCorrect']
    @staticmethod
    def js_vars(player):
        session =player.session
        return dict(
            seedTask=session.config['seedTask'],rows=session.config['rows'],columns=session.config['columns'],minValue=session.config['minValue'],maxValue=session.config['maxValue'],timeGlobal=session.config['timeGlobal'],timeRest=session.config['timeRest'],colorOK=session.config['colorOK'],colorNotOK=session.config['colorNotOK'],subjectID=player.id_in_subsession,
            
        )


    @staticmethod
    def before_next_page(player, timeout_happened):
        session =player.session
        player.payoff=player.numbersTablesCorrect*session.config['payByTable']
#class ResultsWaitPage(WaitPage):
#    pass


class ResultsTasks(Page): 
    pass

class Survey(Page): 
    pass

class EndExperiment(Page): 
    pass

class EndNotConsent(Page): 
    pass



page_sequence = [Consent,Instructions,TaskPractice,PreTask,Task,ResultsTasks,Survey,EndExperiment,EndNotConsent]
